//
//  TempViewController.swift
//  HiveMonitor
//
//  Created by Zachary Cole on 4/14/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit
import Charts

class TempViewController: UIViewController {
    @IBOutlet weak var lineChart: LineChartView!
    var dates: [String] = []
    var reads: [Double] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MM-dd-YYYY"
        
        for reading in readings {
            let dateString = dateFormatter.stringFromDate(reading["date"] as! NSDate)
            dates.insert(dateString, atIndex: dates.startIndex)
            reads.insert(Double(reading["temp"] as! String)!, atIndex: reads.startIndex)
        }

        setChart(dates, values: reads)
        // Do any additional setup after loading the view.
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        lineChart.noDataText = "Retrieving data for the chart."
        lineChart.xAxis.labelPosition = .Bottom
        lineChart.descriptionText = "Average daily temperature"
        lineChart.backgroundColor = UIColor(red: 0, green: 0.9882, blue: 0.8902, alpha: 1.0) /* #00fce3 */

        lineChart.xAxis.labelWidth = 1
        lineChart.xAxis.labelRotationAngle = -50
        lineChart.xAxis.drawGridLinesEnabled = false
        lineChart.leftAxis.drawGridLinesEnabled = false
        lineChart.rightAxis.drawGridLinesEnabled = false
        lineChart.xAxis.setLabelsToSkip(0)
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntries.append(dataEntry)
        }
        
        let chartDataSet = LineChartDataSet(yVals: dataEntries, label: "Degrees F")
        
        chartDataSet.drawCirclesEnabled = true;
        chartDataSet.drawCircleHoleEnabled = false;
        chartDataSet.setColor(NSUIColor.blueColor())
        chartDataSet.setCircleColor(NSUIColor.blueColor())
        
        let chartData = LineChartData(xVals: dates, dataSet: chartDataSet)
        
        lineChart.data = chartData
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
