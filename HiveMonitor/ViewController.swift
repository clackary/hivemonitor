//
//  ViewController.swift
//  HiveMonitor
//
//  Created by Zachary Cole on 3/21/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

    var readings = [[String:AnyObject]()]

class ViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var humLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        //Add test data
//        post(temp, hum: hum, date: str);
//        post("95", hum: "24", date: "\"2016-04-03T12:00Z\"");
//        post("92", hum: "22", date: "\"2016-04-04T12:00Z\"");
//        post("90", hum: "23", date: "\"2016-04-05T12:00Z\"");
//        post("80", hum: "24", date: "\"2016-04-06T12:00Z\"");
//        post("82", hum: "25", date: "\"2016-04-07T12:00Z\"");
        
//        post("70", hum: "23", date: "\"2016-04-17T18:18Z\"");

        get("") { (result) in
            dispatch_async(dispatch_get_main_queue()) {
                // update some UI
                self.tempLabel.text = (readings.first!["temp"] as? String)! + "\u{00B0} F"
                self.humLabel.text = (readings.first!["humidity"] as? String)! + "%"
            }
        }
        
        imgView.image = UIImage(named: "bee_logo4.png")
        self.view.addSubview(imgView)
    }
    
    func sort() -> Void {
        //get rid of extraneous dictionary with no values at index 0
        readings.removeAtIndex(0)
        
        readings.sortInPlace {
            item1, item2 in
            let date1 = item1["date"] as! NSDate
            let date2 = item2["date"] as! NSDate
            return date1 > date2
        }
    }
    
    func get(input: String, completion: (result: String) -> Void)  {
        let url:NSURL = NSURL(string: "https://oak-hive1.herokuapp.com/parse/classes/oakhive")!
        let session = NSURLSession.sharedSession()
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.cachePolicy = NSURLRequestCachePolicy.UseProtocolCachePolicy
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("oak-hive1", forHTTPHeaderField: "X-Parse-Application-ID")
        
        let task = session.dataTaskWithRequest(request) { (data, response, error) -> Void in
            
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers) as? Dictionary <String,AnyObject>
                var hum: String = String()
                var temp: String = String()
                var date: NSDate = NSDate()
                
                if let results = json!["results"] as? [[String: AnyObject]] {
                    for result in results {
                        // Get humidity reading
                        hum = (result["humidity"] as? String)!
                        // Get temp reading
                        temp = (result["temp"] as? String)!
                        // Get date of reading
                        if let dateStr = result["date"] as? String {
                            let dateFormatter = NSDateFormatter()
                            dateFormatter.dateFormat = "\"yyyy-MM-dd'T'HH:mm'Z'\""
                            date = dateFormatter.dateFromString(dateStr)!
                        }
                        
                        var readingDict = [String:AnyObject]()
                        readingDict.updateValue(hum, forKey: "humidity")
                        readingDict.updateValue(temp, forKey: "temp")
                        readingDict.updateValue(date, forKey: "date")
                        
                        // Add to readings (array of dictionaries{temp, hum, date})
                        readings.append(readingDict)
                    }
                }
                
                self.sort()
                completion(result: "done")
            }
            catch {
            }
        }
        
        task.resume()
    }
    
    func post(temp: String, hum: String, date: String) {
        let json = ["temp": temp, "humidity": hum, "date": date]
        
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted)
            let url:NSURL = NSURL(string: "https://oak-hive1.herokuapp.com/parse/classes/oakhive")!
            let request = NSMutableURLRequest(URL: url, cachePolicy: NSURLRequestCachePolicy.UseProtocolCachePolicy, timeoutInterval: 60)
            request.HTTPMethod = "POST"
            
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("oak-hive1", forHTTPHeaderField: "X-Parse-Application-ID")
            request.HTTPBody = jsonData
            
            let session = NSURLSession.sharedSession().dataTaskWithRequest(request){ data, response, error in
                if error != nil{
                    print("Error -> \(error)")
                    return
                }
                
                do {
                    let result = try NSJSONSerialization.JSONObjectWithData(data!, options: []) as? [String:AnyObject]
                    
                    print("Result -> \(result)")
                    
                } catch {
                    print("Error -> \(error)")
                }
            }
            
            session.resume()
            
        } catch {
            //print error
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension NSDate: Comparable { }

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.isEqualToDate(rhs)
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedAscending
}
